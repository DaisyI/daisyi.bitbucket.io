Setup Guide
1. Install VS code (other stuff would work but this is what i'm using)

2.	Install node: 
https://nodejs.org/en/download/

3.	Install Typescript
npm install -g typescript
check version:
tsc –v

4.	Install TS-Node
npm install -g ts-node

TS-Node is a Node. js package that we can use to execute TypeScript files or run TypeScript in a REPL environment. To compile and run TypeScript directly from the command line, we need to install the compiler and ts-node globally

5.	Check tsc is runnable from inside VS code. Click Terminal on the menu to bring up the terminal. If not, run in Powershell:
PS C:\> Set-ExecutionPolicy RemoteSigned

From inside VS code  (running as administrator) 

6.1	Install webpack: npm install webpack
6.2 Install jquery: npm install jquery

7. To run with debugging:
	Open debugging tab: Ctrl + Shift + D
	From the  top left dropdown select "Debug with node"
	Press F5
	
To build production bundle.js (currently running on trial51 https://daisyi.bitbucket.io/pl2/build/bundle.js)
	Open debugging tab: Ctrl + Shift + D
	From the  top left dropdown select "Build Production"
	Ctrl F5
	Build/bundle.js should be updated.
	
https://google.github.io/styleguide/tsguide.html
