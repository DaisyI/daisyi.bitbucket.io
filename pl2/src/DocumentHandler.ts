//here we'll use the object to find the distrubutors and then build the html.
//manipulate other parts of the page.
export class DocumentHandler {

  private distributors: IDistributorInfo[]
  private partnerButton: string
  private partnerContainer: string

  constructor() {
    this.partnerButton = '<div style="text-align: right;padding: 10px 0;"><a class="btn btn-secondary" href="#partner-area">See our Authorized partners</a></div>';
    this.partnerContainer =
      `<div>
  <style type="text/css">
    @media screen and (max-width:576px) {
      .widget-anchor {display: block;height: 110px;margin-top: -110px;visibility: hidden;}
      .card_padding { padding: 6px; }
      .card_distributor { display: flex; flex-wrap: wrap; border: solid 2px #f8f8f8; padding: 5px; height: 75px; transition: border 0.5s; }
      .card_distributor:hover { text-decoration: none; border: solid 2px #e5001236; }
      .card_distributor:focus { text-decoration: none; outline: none; border: solid 2px #dcdcdc; }
      .card_distributor--shadow { border: none; box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;}
      .card_distributor--shadow:hover { text-decoration: none; border: none; }
      .card_distributor--shadow:focus { text-decoration: none; outline: none; border: none; }
      .card_distributor__logo { flex: 0 0 100%; padding: 5px; height: 65px; }
      .card_distributor__logo__img { width: 100%; height: 100%; object-fit: scale-down; }
      .card_distributor__description { display:none; }
      .card_distributor__anchor { display:none; }
    }
    @media screen and (min-width:576px) {
      .widget-anchor {display: block;height: 160px;margin-top: -160px;visibility: hidden;}
      .card_padding { padding: 10px; }
      .card_distributor { display: flex; border: solid 2px #f8f8f8; padding: 5px; height: 110px; transition: border 0.5s; }
      .card_distributor:hover { text-decoration: none; border: solid 2px #e5001236; }
      .card_distributor:focus { text-decoration: none; outline: none; border: solid 2px #dcdcdc; }
      .card_distributor--shadow { border: none; box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;}
      .card_distributor--shadow:hover { text-decoration: none; border: none; }
      .card_distributor--shadow:focus { text-decoration: none; outline: none; border: none; }
      .card_distributor__logo { flex: 0 0 100px; padding: 5px; }
      .card_distributor__logo__img { width: 100%; height: 100%; object-fit: scale-down; }
      .card_distributor__description { flex: 0 1 auto; margin: 10px; color: #626272; font-size: .9rem; overflow: hidden; }
      .card_distributor__anchor { flex: 0 0 0; padding: 5px; font-size: 1.5rem; line-height: 86px; color: #e60012; }
    }
  </style>
  <span id="partner-area" class="widget-anchor"></span>
  <h3 class="m-t-2 header">Authorized partners</h3>
  <div class="row" id="partner-list">
  </div>
</div>`;
  }

  public IsLoggedIn(): boolean {
    return (document.querySelector(".primary-items").querySelector(".text-uppercase") == null)
  }

  private GetDistributorCards(): string {

    var distributor_cards = "";

    this.distributors.forEach(distinfo => {

      let distributor_card =
        `<div class="col-xs-6 card_padding">
        <a class="card_distributor card_distributor--shadow" href="${distinfo.distributor_webstore_url}" target="_blank">
            <div class="card_distributor__logo">
            <img class="card_distributor__logo__img" src="${distinfo.distributor_logo_url}">
            </div>
            <div class="card_distributor__description">${distinfo.distributor_description}</div>
            <div class="card_distributor__anchor">
            <i class="fas fa-caret-right"></i>
            </div>
        </a>
        </div>`;

      distributor_cards = distributor_cards + distributor_card;
    });

    return distributor_cards;
  }

  public InsertLayout() {
    let delivery_remark_area = document.querySelector(".page-content :first-child");
    delivery_remark_area.insertAdjacentHTML('afterend', this.partnerButton);
    let checkout_area = document.querySelector(".form-order");
    checkout_area.insertAdjacentHTML('beforeend', this.partnerContainer);
  }

  public InsertUnavailableMessage() {
    document.querySelector("#partner-list").innerHTML = "";
    document.querySelector(".m-t-2.header").innerHTML = 'This site is unable to service those outside the Americas. Please direct your inquiry to our <a href="https://www.mitsubishielectric.com/fa/business/overseas/index.html" target="_blank">Global Website</a> and select the local Mitsubishi Electric office that services your location for assistance.';
  }

  public InsertDistributorsIntoDocument(distributorInfo: IDistributorInfo[], isLocationSpecific: boolean) {
    if (isLocationSpecific) {
      document.querySelector(".m-t-2.header").innerHTML = 'Authorized partners';
    }

    this.distributors = distributorInfo;

    let partner_list = document.querySelector("#partner-list");
    partner_list.innerHTML = ""
    partner_list.insertAdjacentHTML('beforeend', this.GetDistributorCards());
  }

  public ExtractPostcode(): string {

    var title: string;
    var isFound: number;
    var addressDropdown: Element = document.querySelector(".btn.dropdown-toggle")

    title = addressDropdown.getAttribute("title");

    if (title == null) {
      //title attribute not found
      console.log("title attribute not found")
      return "";
    }

    var isFound = title.search("United States of America");
    if (isFound < 0) {

      //not american
      console.log("not american")
      return "NONAMERICA";
    }

    var titlePieces = title.replace(' United States of America (USA)', '').split(" ");
    var postcode = titlePieces[titlePieces.length - 1].substring(0, 5);

    return postcode;

  }

  public HidePlaceOrder() {

    console.log("Action: hide po and place order button");
    let po_number_area = document.querySelector("#po_number").parentElement;
    let place_order_button : HTMLInputElement = document.querySelector(".btn.btn-secondary.btn-checkout");
    po_number_area.style.display = "none"
    place_order_button.style.display = "none"
  }

  public BlurPartners() {

    document.querySelector("#partner-list").setAttribute('style', '-webkit-filter: blur(5px);-moz-filter: blur(5px);-o-filter: blur(5px);-ms-filter: blur(5px);-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;-o-user-select: none;user-select: none;');

    (document.querySelector(".m-t-2.header") as HTMLInputElement).innerText = 'Login to view our Authorized partners';
    $.each($('.card_distributor--shadow'), function (a, b) { console.log($(b).removeAttr('href')) });
  }

}



