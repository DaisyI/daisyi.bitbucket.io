// extend the Window interface to include the missing method.
export module Extensions {
    export interface Window { 
        WebKitMutationObserver : new (callback: Function) => WebKitMutationObserver
    }

    export interface WebKitMutationObserver {
     observe(checkout_area: Element, arg1: { subtree: boolean; childList: boolean; }): any; 
   }

}

