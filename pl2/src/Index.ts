import { Extensions } from './Extensions';
import { PartnerLocator } from './PartnerLocator';

//the URL where the json file with all the data we need is stored.
const jsonUrl = `https://daisyi.bitbucket.io/partner-locatortest.json`;

//workaround because typescript version doesn't have webkitmutation type.
declare var window: Extensions.Window & Window

// do the work after everything was loaded (DOM, media elements)  
window.onload = function () {

    if (window.location.pathname != "/cart/edit") {
        return
    }
    console.log("we should be on cart edit only now")

    $.get(jsonUrl).done((response) => {
        var partnerLocator: PartnerLocator = new PartnerLocator(response)
        partnerLocator.InitializeThePage();
        partnerLocator.HidePlaceOrder();
        partnerLocator.AddPartnersToPage();

        //Monitor change on parent div of place order button and PO text field.
        //Once these elements are added by platform. Our script will remove it.

        //We also monitor if the user changes the delivery dropdown postcode

        //Unable to make the listeners specific to the product changes and dropdown changes (dom is getting updated 
        //by the platform)?
        //so have one observer that does both the hiding of place order and the repopulating of the partners.
        var checkout_area = document.querySelector(".product-list")
        var checkout_area_listener = new window.WebKitMutationObserver(function () {
       
           partnerLocator.HidePlaceOrder()
           partnerLocator.AddPartnersToPage()

        });
        checkout_area_listener.observe(checkout_area, { subtree: false, childList: true }); 

        //3. You can see that I use again script to remove PO and place order button. 
        // This is repetitive with script on JS snippet area. Can we call this function from snippet 
        // or vice versa?
    }).fail((errorResponse) => { console.log(errorResponse.responseText) })

}

