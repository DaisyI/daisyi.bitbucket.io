import { AjaxClient } from "./AjaxClient";

//here we'll use the object to find the distrubutors and then build the html..
export class JsonHandler {
    private distributor_codes: string[]
    private distributors: IDistributorInfo[]
    private partnerLocator: IPartnerLocator
    private ajaxClient : AjaxClient

constructor(data: IPartnerLocator)
{
    this.distributor_codes = [];
    this.distributors = [];
    this.partnerLocator = data;
    this.ajaxClient = new AjaxClient();
}

public GetAllDistributors(){

    return this.partnerLocator.distributor_info;
}

public GetDistributorsByPostcode(userPostcode: string): IDistributorInfo[] {

  
    this.distributor_codes = this.GetDistributorCodesByPostcode(this.partnerLocator.manufacturer_sales_regions, userPostcode);
 
    if (this.distributor_codes.length < 1) {
        console.log("WARNING: no distributor codes found for postcode: " + userPostcode);
    }
    else {
        this.distributors = this.GetDistributorsByCode(this.distributor_codes, this.partnerLocator.distributor_info);
        //just to test its working..
        console.log(this.distributors.length + " distributors found for: " + userPostcode);
    }
    return this.distributors;
}

private GetDistributorsByCode(distributor_codes: string[], data: IDistributorInfo[]) {

        //covert the codes into the full distributor data object
        this.distributors = data
            .filter((di) => distributor_codes
                .some(dc => dc == di.distributor_code));

        if (this.distributors.length < 1) {
            console.log("WARNING: the distributor codes were not found in the distributor info object!");        
        }
        else if (this.distributors.length < distributor_codes.length){
            console.log("WARNING: some of the distributor codes were not found in the distributor info object!");         
        }

        return this.distributors;
}

private GetDistributorCodesByPostcode(data: IManufacturerSalesRegion[], userPostcode: string) {

        data.forEach(region => {

            // if we find the post code in this region
            if (region.region_postcodes.some(post => post == userPostcode)) { //store these distributor codes
                this.distributor_codes = region.distributor_codes;
            }
        });
        return this.distributor_codes;
}

}
