import { JsonHandler } from "./JsonHandler";
import { DocumentHandler } from './DocumentHandler';

//here we'll use the object to find the distrubutors and then build the html..
export class PartnerLocator {
    public IsLocationSpecific: boolean
    public IsLoggedIn: boolean;
    private documentHandler: DocumentHandler
    private jsonHandler: JsonHandler
    private distributors: IDistributorInfo[]
    

    public constructor(partnerLocator: IPartnerLocator) {
        this.IsLocationSpecific = false;      
        this.IsLoggedIn = false;
        this.documentHandler = new DocumentHandler()
        this.IsLoggedIn = this.documentHandler.IsLoggedIn();
        this.jsonHandler = new JsonHandler(partnerLocator)
    }

    public InitializeThePage() {
        this.documentHandler.InsertLayout()
        this.HidePlaceOrder()
        this.AddPartnersToPage()
    }

    public HidePlaceOrder() {
        this.documentHandler.HidePlaceOrder()
    }

    public AddPartnersToPage() {        

        var userPostcode = "";

        if (this.IsLoggedIn)
        {
            userPostcode = this.documentHandler.ExtractPostcode();
        }     

        if (userPostcode == "") {
            this.distributors = this.jsonHandler.GetAllDistributors()
            this.documentHandler.InsertDistributorsIntoDocument(this.distributors, this.IsLocationSpecific)

        } else if (userPostcode == "NONAMERICA") {
            this.documentHandler.InsertUnavailableMessage();
        }
        else {
            this.IsLocationSpecific = true;
            this.distributors = this.jsonHandler.GetDistributorsByPostcode(userPostcode)
            this.documentHandler.InsertDistributorsIntoDocument(this.distributors, this.IsLocationSpecific)
        }

        //blur partners to encourage log in/sign up
        if (!this.IsLoggedIn){
            this.documentHandler.BlurPartners();
        }
    }
}