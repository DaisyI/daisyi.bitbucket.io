interface IDistributorInfo 
{
    distributor_name: string;
    distributor_code: string;
    manufacturer_region_code: string;
    distributor_description: string;
    distributor_logo_url: string;
    distributor_phone_number: string;
    distributor_webstore_url: string;
}
