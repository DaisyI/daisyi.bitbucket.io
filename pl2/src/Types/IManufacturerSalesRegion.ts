interface IManufacturerSalesRegion
{
    manufacturer_region : string;
    manufacturer_region_code : string;
    region_postcodes : string[];
    distributor_codes : string[];
} 

